<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Http\Services\Identifier;

class IdentifierTest extends TestCase
{
    /**
     * @var Identifier $identifier
     */
    private $identifier;

    public function setUp(): void
    {
        parent::setUp();
        $this->identifier = app(Identifier::class);
    }

    public function testException()
    {
        $entriesSet = [
            ['   ', 'OLABISI'],
            ['', ''],
            ['IDOWU', '  '],
        ];

        array_walk($entriesSet, function ($entries) {
            $this->expectException(Exception::class);
            $this->expectExceptionMessage('Invalid input entries!');

            $this->identifier->identifyPerson($entries[0], $entries[1]);
        });
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFailure()
    {
        $entriesSet = [
            ['ZAINAB', 'OLABISI'],
            ['IDOWU EBUNOLUWA AGU SARAH', 'SARAH, EBUNOLUWA OLABISI ABDULSALAM'],
            ['IDOWU', 'IDOWU EBUNOLUWA'],
        ];

        array_walk($entriesSet, function ($entries) {
            $expected = $this->identifier->identifyPerson($entries[0], $entries[1]);
            $this->assertFalse($expected);
        });
    }

    public function testTruthfully()
    {
        $entriesSet = [
            ['IDOWU', 'IDOWU'],
            ['IDOWU EBUNOLUWA', 'EBUNOLUWA IDOWU'],
            ['IDOWU EBUNOLUWA', 'IDOWU EBUNOLUWA'],
            ['IDOWU SARAH EBUNOLUWA', 'EBUNOLUWA IDOWU'],
            ['IDOWU EBUNOLUWA SARAH', 'SARAH, EBUNOLUWA IDOWU'],
            ['Agu Adline', 'ADLINE'],
            ['OLISEMEKA ADLINE', 'ADLINE AGU OLISEMEKA'],
            ['ZAINAB, OLABISI ABDULSALAM', 'ZAINAB OLABISI, Abdulsalam'],
        ];

        array_walk($entriesSet, function ($entries) {
            $expected = $this->identifier->identifyPerson($entries[0], $entries[1]);
            $this->assertTrue($expected);
        });
    }
}
