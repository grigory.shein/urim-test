<?php
namespace App\Http\Services;

use http\Exception;
use function PHPUnit\Framework\throwException;

class Identifier
{
    /**
     * @var int $limit
     */
    private $limit;

    public function __construct() {
        $this->limit = 66;
    }

    /**
     * @param string $name
     * @param string $comparable
     * @return bool
     */
    public function identifyPerson(string $name, string $comparable): bool
    {
        $nameToLowerCase       = strtolower(trim($name));
        $comparableToLowerCase = strtolower(trim($comparable));

        if (empty($nameToLowerCase) || empty($comparableToLowerCase))
            abort(500, 'Invalid input entries!');

        if ($nameToLowerCase === $comparableToLowerCase) return true;
        if ($this->isSubString($nameToLowerCase, $comparableToLowerCase)) return true;

        $nameWordSet = $this->getWordsFromEntry($nameToLowerCase);
        $comparableWordSet = $this->getWordsFromEntry($comparableToLowerCase);

        $matches = array_reduce($nameWordSet, function ($carry, $word) use ($comparableWordSet) {
            $carry += (int) in_array($word, $comparableWordSet);
            return $carry;
        }, 0);

        $wordsCount = max(count($nameWordSet), count($comparableWordSet));

        return $this->enoughMatches($wordsCount, $matches);
    }


    /**
     * @param string $entry
     * @return false|string[]
     */
    private function getWordsFromEntry(string $entry)
    {
        return preg_split('(,?\s)', $entry);
    }

    /**
     * @param string $string
     * @param $subString
     * @return false|int
     */
    private function isSubString(string $string, $subString)
    {
        return preg_match('/' . $subString . '/', $string);
    }

    /**
     * @param int $count
     * @param int $matches
     * @return bool
     */
    private function enoughMatches(int $count, int $matches)
    {
        return $matches / $count * 100 >= $this->limit;
    }
}
