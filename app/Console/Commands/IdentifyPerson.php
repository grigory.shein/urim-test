<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Services\Identifier;

class IdentifyPerson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'person:identify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A command that compares two strings with a name and determines if they are the same user';

    /**
     * IdentifyPerson constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Identifier $identifier
     * @return bool
     */
    public function handle(Identifier $identifier): bool
    {
        $firstEntryString = $this->ask('Enter the first name option');
        $secondEntryString = $this->ask('Enter the second name option');

        $res = $identifier->identifyPerson($firstEntryString, $secondEntryString);

        $res
            ? $this->info('This is the same user!')
            : $this->warn('These are different users');

        return $res;
    }
}
